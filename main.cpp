#include <iostream>
#include <vector>
#include <memory>

using namespace std;

class AnyValue {
public:
     virtual ~AnyValue() = 0;
};
AnyValue::~AnyValue(){}

template <class T>
class AnyValueImpl : public AnyValue {
public:
    AnyValueImpl(const T& t) {
        _t = t;
    }

    operator T() const {
        return _t;
    }

    T _t;
};

class Any {
public:
    template <class T>
    Any(const T& t) :
         _value(Value(new AnyValueImpl<T>(t))) {}

    template <class T>
    operator T() const {
        return static_cast<AnyValueImpl<T>*>(_value.get())->_t;
    }
private:
    typedef std::unique_ptr<AnyValue> Value;

    Value _value;
};


int main() {
    std::vector<Any> l;
    l.push_back(1);
    l.push_back(3.14);

    // for(auto i : l) {
    //     std::cout << i << std::endl;
    // }
    std::cout << static_cast<int>(l[0]) << endl;
    std::cout << static_cast<double>(l[1]) << endl;

    return 0;
}